import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import java.io.PrintStream;

public class GameMockitoTest {

    @Mock
    Player player1;

    @Mock
    Player player2;

    @Mock
    Machine machine;

    @Mock
    PrintStream printStream;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void basicMockGame(){

        int[] expectedScores = {2,2};

        Mockito.when(player1.makeAMove()).
                thenReturn(MoveType.Moves.COOPERATE);
        Mockito.when(player2.makeAMove()).
                thenReturn(MoveType.Moves.COOPERATE);
        Mockito.when(machine.calculate(player1.makeAMove(), player2.makeAMove())).
                thenReturn(expectedScores);


        Assert.assertEquals(MoveType.Moves.COOPERATE, player1.makeAMove());
        Assert.assertEquals(MoveType.Moves.COOPERATE, player2.makeAMove());
        Assert.assertEquals(expectedScores, machine.calculate(player1.makeAMove(),
                player2.makeAMove()));

        Game game = new Game(player1, player2, machine, printStream);
        game.play();


    }

}
