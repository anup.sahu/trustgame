public interface PlayerBehaviour {
     MoveType.Moves makeAMove();
}
