import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Game {

    Player player1;
    Player player2;
    Machine machine;
    PrintStream printStream;
    int numberOfRounds;

    public void setNumberOfRounds(int numberOfRounds) {
        this.numberOfRounds = numberOfRounds;
    }

    public Game(Player player1, Player player2, Machine machine, PrintStream printStream) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.printStream = printStream;
    }

    public int[] play() {

        printStream.print("\nPlayer1 is making move");
        MoveType.Moves player1Move = player1.makeAMove();
        printStream.print("\nPlayer1 moved:" + player1Move);
        printStream.print("\nPlayer2 is making move");
        MoveType.Moves player2Move = player2.makeAMove();
        printStream.print("\nPlayer2 moved:" + player2Move);

        int[] result = machine.calculate(player1Move, player2Move);
        int player1score = result[0];
        int player2score = result[1];

        printStream.print("\nRound Score:" + player1score + " ," + player2score);
        player1.scores.add(player1score);
        player2.scores.add(player2score);
        printStream.print("\nPlayer1 Total: " + sum(player1.scores) + " Player2 Total: " + sum(player2.scores));
        numberOfRounds -= 1;
        if (numberOfRounds != 0) play();
        return new int[]{sum(player1.scores), sum(player2.scores)};
    }

    private int sum(ArrayList<Integer> arrayList) {
        int sum = 0;
        for (Integer a : arrayList)
            sum += a;
        return sum;
    }

}
